# Security groups

resource "aws_security_group" "main_sg" {
  name_prefix = "-sg-"
  description = "Allow public access to server"

  tags = {
    Name = "-sg"
  }
}

resource "aws_security_group_rule" "egress" {
  description = "Egress"
  type = "egress"
  from_port = 0
  to_port = 0
  protocol = "-1"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = aws_security_group.main_sg.id
}

resource "aws_security_group_rule" "ssh" {
  description = "SSH access"
  type = "ingress"
  from_port = 22
  to_port = 22
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = aws_security_group.main_sg.id
}

resource "aws_security_group_rule" "http" {
  description = "HTTP"
  type = "ingress"
  from_port = 80
  to_port = 80
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = aws_security_group.main_sg.id
}

resource "aws_security_group_rule" "custom" {
  description = "Create for Node Application"
  type = "ingress"
  from_port = 3000
  to_port = 3000
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = aws_security_group.main_sg.id
}


resource "aws_security_group_rule" "spring" {
  description = "Create for Node Application"
  type = "ingress"
  from_port = 8080
  to_port = 8080
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = aws_security_group.main_sg.id
}
