## ATARC Project 

An IaC/GitOps Terraform project in GitLab to leverage built in GitLab functionality.  

This project creates a GitOps pipeline to perform `init`, `validate`, `build`, and `deploy` stages to create an EC2 environment.

## Purpose

Within the ATARC project, the purpose of this IaC/GitOps project is to provision and start an AWS EC2 instance.  

## What is not in this project

This project is just to spin up an EC2 instance in AWS.  It is not meant to deploy a project, though using multi project pipelines, this project can be leveraged to do so.  Given the expensive nature of the spinup and down, this project is meant to just create an instance and keep it ready for a deployment that happens from another location (GitLab or other).

## Additional configuration necessary

#### AWS Secrets

`AWS_SECRET_ACCESS_KEY` and `AWS_ACCESS_KEY_ID` need to be set to the correct values in Settings > CI/CD > Variables.  That way the secrets will be hidden.  Secrets should never be commited as part of any configuration file as a general security rule.

#### AWS Route 53 record for external access
An AWS Route 53 record should be created and tied to a domain name.  This will provide a way to ingress the EC2 from the web.  This can be configured in the `ec2.tf` file

#### AWS Key pair
An AWS Key pair with the name `gitlab-atarc` should be created for appropriate SSH access to deploy things into the instance.

