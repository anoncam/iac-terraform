#Required input vars that need to be passed into this module
variable "env" {}
variable "ami_id" {}
variable "app_instance_type" {}
variable "ssh_key_name" {}
variable "app_instance_iam_profile" {}
variable "app_instance_subnet_ids" {type = "list"}
variable "jump_server_subnet_ids" {type = "list"}
variable "vpc_id" {}
variable "max_instances" {}
variable "min_instances" {}
variable "desired_instances" {}
variable "percent_capacity_increase" {}
variable "jump_instance_type" {}
variable "sg_database_id" {}
variable "elb_logging_bucket" {}
variable "user_data" {}
variable "gold_ami_id" {}
variable "gold_ami_description" {}
variable "autoscale_group_wait" {}
variable "healthcheck_url" {}
variable "open_http_security_group_id" {}
variable "private_subnet_ids" {type = "list"}
