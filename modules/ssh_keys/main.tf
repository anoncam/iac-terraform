resource "null_resource" "authorized_keys_file" {
  provisioner "local-exec" {
    command = "scp -o StrictHostKeyChecking=no -i ~/.ssh/${var.private_key}.pem ${var.authorized_keys_file} ec2-user@${var.jumpserver_ip}:/tmp/"
  }

  provisioner "local-exec" {
    command = "ssh -i ~/.ssh/${var.private_key}.pem ec2-user@${var.jumpserver_ip} 'chmod 600 /tmp/authorized_keys && mv /tmp/authorized_keys ~/.ssh/authorized_keys'"
  }
}


resource "null_resource" "jumpserver_private_key" {
  provisioner "local-exec" {
    command = "scp -i ~/.ssh/${var.private_key}.pem ~/.ssh/${var.private_key}.pem ec2-user@${var.jumpserver_ip}:/tmp/id.rsa"
  }

  provisioner "local-exec" {
    command = "ssh -i ~/.ssh/${var.private_key}.pem ec2-user@${var.jumpserver_ip} 'chmod 600 /tmp/id.rsa && mv /tmp/id.rsa ~/.ssh/'"
  }
}

resource "null_resource" "ssh_client_config" {
  provisioner "local-exec" {
    command = "scp -i ~/.ssh/${var.private_key}.pem ../../modules/ssh_keys/client_config ec2-user@${var.jumpserver_ip}:/home/ec2-user/.ssh/config"
  }

  provisioner "local-exec" {
    command = "ssh -i ~/.ssh/${var.private_key}.pem ec2-user@${var.jumpserver_ip} 'chmod 640 /home/ec2-user/.ssh/config'"
  }
}
