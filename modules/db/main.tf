resource "aws_security_group" "sg_database" {
  name = "hello-world-${lower(var.env)}-DatabaseSecurityGroup"
  description = "Database security group"
  vpc_id = "${var.vpc_id}"
}

resource "aws_security_group_rule" "splunk" {
  type        = "ingress"
  description = "Splunk"
  from_port   = "3306"
  to_port     = "3306"
  protocol    = "tcp"
  cidr_blocks = ["10.244.96.0/19"]
  security_group_id = "${aws_security_group.sg_database.id}"
}

resource "aws_security_group_rule" "openvpn" {
  type        = "ingress"
  description = "OpenVPN"
  from_port   = "3306"
  to_port     = "3306"
  protocol    = "tcp"
  cidr_blocks = ["34.198.214.191/32"]
  security_group_id = "${aws_security_group.sg_database.id}"
}

resource "aws_security_group_rule" "egress" {
  type        = "egress"
  description = "Allow all egress"
  from_port   = "0"
  to_port     = "0"
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.sg_database.id}"
}

resource "aws_db_subnet_group" "subnet_group" {
  name = "${var.subnet_group_name}"
  subnet_ids = ["${var.db_instance_subnet_ids}"]
}

resource "aws_db_instance" "db" {
  allocated_storage = "${var.allocated_storage_size}"
  engine = "mysql"
  engine_version = "${var.mysql_version}"
  instance_class = "${var.instance_class}"
  identifier = "${var.identifier}"
  snapshot_identifier = "${var.snapshot_id}"
  db_subnet_group_name = "${aws_db_subnet_group.subnet_group.id}"
  parameter_group_name = "${var.parameter_group_name}"
  backup_retention_period = "${var.backup_retention_period}"
  backup_window = "${var.backup_window}"
  copy_tags_to_snapshot = "${var.copy_tags_to_snapshot}"
  iops = "${var.iops}"
  kms_key_id = "${var.kms_key_id}"
  maintenance_window = "${var.maintenance_window}"
  multi_az = "${var.multi_az}"
  storage_encrypted = true
  vpc_security_group_ids = ["${aws_security_group.sg_database.id}"]

  tags {
    "workload-type" = "production",
    "cpm backup" = "Weekly Monthly Annually"
    "Application" = "hello world"
  }
}
