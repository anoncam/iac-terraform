resource "aws_s3_bucket" "logging" {
  bucket = "hello-world-logging-${lower(var.env)}"
  acl    = "log-delivery-write"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = "${var.encryption_key_arn}"
        sse_algorithm     = "aws:kms"
      }
    }
  }

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AWSCloudTrailAclCheck20150319",
            "Effect": "Allow",
            "Principal": {
                "Service": "cloudtrail.amazonaws.com"
            },
            "Action": "s3:GetBucketAcl",
            "Resource": "arn:aws:s3:::hello-world-logging-${lower(var.env)}"
        },
        {
            "Sid": "AWSCloudTrailWrite20150319",
            "Effect": "Allow",
            "Principal": {
                "Service": "cloudtrail.amazonaws.com"
            },
            "Action": "s3:PutObject",
            "Resource": "arn:aws:s3:::hello-world-logging-${lower(var.env)}/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": "bucket-owner-full-control"
                }
            }
        }
    ]
}
POLICY
}

resource "aws_s3_bucket" "replicated" {
  provider = "aws.us-east-2"
  bucket = "${var.bucket_name}-replicated"
  acl    = "private"
  versioning {enabled = true}

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = "${var.replica_encryption_key_arn}"
        sse_algorithm     = "aws:kms"
      }
    }
  }
}

resource "aws_s3_bucket" "fileservices" {
  bucket = "${var.bucket_name}"
  acl    = "private"
  versioning {enabled = true}
  logging {target_bucket = "${aws_s3_bucket.logging.id}", target_prefix = "server_access/"}

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = "${var.encryption_key_arn}"
        sse_algorithm     = "aws:kms"
      }
    }
  }

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "Policy1510340420501",
    "Statement": [
        {
            "Sid": "DenyIncorrectEncryptionHeader",
            "Effect": "Deny",
            "Principal": "*",
            "Action": "s3:PutObject",
            "Resource": "arn:aws:s3:::${var.bucket_name}/*",
            "Condition": {
                "StringNotEquals": {
                    "s3:x-amz-server-side-encryption": "aws:kms"
                }
            }
        },
        {
            "Sid": "DenyUnEncryptedObjectUploads",
            "Effect": "Deny",
            "Principal": "*",
            "Action": "s3:PutObject",
            "Resource": "arn:aws:s3:::${var.bucket_name}/*",
            "Condition": {
                "Null": {
                    "s3:x-amz-server-side-encryption": "true"
                }
            }
        }
    ]
}
POLICY

replication_configuration {
    role = "${var.iam_role_arn}"

    rules {
      id     = "Entire Bucket"
      prefix = ""
      status = "Enabled"

      destination {
        bucket = "${aws_s3_bucket.replicated.arn}"
        storage_class = "STANDARD"
      }
    }
  }
}

 resource "aws_cloudtrail" "object_level_logging" {
   name = "object_level_logging_for_${var.bucket_name}"
   s3_bucket_name = "${aws_s3_bucket.logging.id}"
   s3_key_prefix = "object-level/"
   enable_log_file_validation = true

   event_selector {
     read_write_type = "All"
     include_management_events = true
     data_resource {
       type = "AWS::S3::Object"
       values = [
          "arn:aws:s3:::${var.bucket_name}/*",
        ]
     }
   }
 }
