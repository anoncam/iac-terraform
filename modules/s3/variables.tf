# Required input vars
variable "env" {}
variable "bucket_name" {}
variable "encryption_key_arn" {}
variable "replica_encryption_key_arn" {}
variable "iam_role_arn" {}
