# Output vars
output "deadletter_queue_name" {
  value = "${aws_sqs_queue.deadletter.name}"
}

output "data_queue_name" {
  value = "${aws_sqs_queue.data.name}"
}

output "file_process_queue_name" {
  value = "${aws_sqs_queue.file-process.name}"
}
