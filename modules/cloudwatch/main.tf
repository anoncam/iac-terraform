resource "aws_cloudwatch_metric_alarm" "DATA_ApproximateAgeOfOldestMessage" {
  alarm_name = "hello-world-${upper(var.env)}-DATA-ApproximateAgeOfOldestMessage"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "3"
  metric_name = "ApproximateAgeOfOldestMessage"
  namespace = "AWS/SQS"
  period = "300"
  statistic = "Average"
  threshold = "180.0"
  alarm_actions = ["${var.sns_arn}"]
  dimensions {"QueueName" = "${var.data_queue}"}
}

resource "aws_cloudwatch_metric_alarm" "FILE_PROCESS_ApproximateAgeOfOldestMessage" {
  alarm_name = "hello-world-${upper(var.env)}-FILE-PROCESS-ApproximateAgeOfOldestMessage"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "3"
  metric_name = "ApproximateAgeOfOldestMessage"
  namespace = "AWS/SQS"
  period = "300"
  statistic = "Average"
  threshold = "180.0"
  alarm_actions = ["${var.sns_arn}", "${var.autoscaling_arn}"]
  dimensions {"QueueName" = "${var.file_process_queue}"}
}

resource "aws_cloudwatch_metric_alarm" "Deadletter_NumberOfMessagesReceived" {
  alarm_name = "HelloWorld${var.env}-DEADLETTER-NumberOfMessagesReceived"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "1"
  metric_name = "NumberOfMessagesReceived"
  namespace = "AWS/SQS"
  period = "300"
  statistic = "Average"
  threshold = "1.0"
  alarm_actions = ["${var.sns_arn}"]
  dimensions {"QueueName" = "${var.deadletter_queue}"}
}

resource "aws_cloudwatch_metric_alarm" "AppCpuAlarm" {
  alarm_name = "hello-world${var.env}AppCpuAlarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "2"
  metric_name = "CPUUtilization"
  namespace = "AWS/EC2"
  period = "300"
  statistic = "Average"
  threshold = "90.0"
  alarm_actions = ["${var.sns_arn}"]
  dimensions {"AutoScalingGroupName" = "${var.autoscaling_name}"}
}

resource "aws_cloudwatch_metric_alarm" "AppHighLatencyAlarm" {
  alarm_name = "hello-world${var.env}AppHighLatencyAlarm"
  alarm_description = "Alerts when several small spikes occur consecutive periods"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "3"
  metric_name = "Latency"
  namespace = "AWS/ELB"
  period = "300"
  statistic = "Average"
  threshold = "1.4"
  alarm_actions = ["${var.autoscaling_arn}", "${var.sns_arn}"]
  dimensions {"LoadBalancerName" = "${var.loadbalancer_name}"}
}

resource "aws_cloudwatch_metric_alarm" "AppHighSingleLatencyAlarm" {
  alarm_name = "hello-world${var.env}AppHighSingleLatencyAlarm"
  alarm_description = "Alerts when large and sustained latency has occurred in a single period"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "1"
  metric_name = "Latency"
  namespace = "AWS/ELB"
  period = "300"
  statistic = "Average"
  threshold = "3"
  alarm_actions = ["${var.autoscaling_arn}", "${var.sns_arn}"]
  dimensions {"LoadBalancerName" = "${var.loadbalancer_name}"}
}

resource "aws_cloudwatch_metric_alarm" "AppNoBackendAlarm" {
  alarm_name = "hello-world${var.env}AppNoBackendAlarm"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods = "3"
  metric_name = "HealthyHostCount"
  namespace = "AWS/ELB"
  period = "60"
  statistic = "Average"
  threshold = "0.0"
  alarm_actions = ["${var.sns_arn}"]
  dimensions {"LoadBalancerName" = "${var.loadbalancer_name}"}
}

resource "aws_cloudwatch_metric_alarm" "AppSpilloverCountAlarm" {
  alarm_name = "hello-world${var.env}AppSpilloverCountAlarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "1"
  metric_name = "SpilloverCount"
  namespace = "AWS/ELB"
  period = "60"
  statistic = "Maximum"
  threshold = "3.0"
  alarm_actions = ["${var.sns_arn}"]
  dimensions {"LoadBalancerName" = "${var.loadbalancer_name}"}
}

resource "aws_cloudwatch_metric_alarm" "AppStatusCheckAlarm" {
  alarm_name = "hello-world${var.env}AppStatusCheckAlarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "1"
  metric_name = "StatusCheckFailed"
  namespace = "AWS/EC2"
  period = "60"
  statistic = "Maximum"
  threshold = "1.0"
  alarm_actions = ["${var.sns_arn}"]
  dimensions {"AutoScalingGroupName" = "${var.autoscaling_name}"}
}

resource "aws_cloudwatch_metric_alarm" "AppSurgeQueueLengthAlarm" {
  alarm_name = "hello-world${var.env}AppSurgeQueueLengthAlarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "1"
  metric_name = "SurgeQueueLength"
  namespace = "AWS/ELB"
  period = "60"
  statistic = "Maximum"
  threshold = "3.0"
  alarm_actions = ["${var.sns_arn}"]
  dimensions {"LoadBalancerName" = "${var.loadbalancer_name}"}
}

resource "aws_cloudwatch_metric_alarm" "JumpCpuAlarm" {
  alarm_name = "hello-world${var.env}JumpCpuAlarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "3"
  metric_name = "CPUUtilization"
  namespace = "AWS/EC2"
  period = "300"
  statistic = "Average"
  threshold = "90.0"
  alarm_actions = ["${var.sns_arn}"]
  dimensions {"InstanceId" = "${var.jump_server_id}"}
}

resource "aws_cloudwatch_metric_alarm" "JumpDiskUsageAlarm" {
  alarm_name = "hello-world${var.env}JumpDiskUsageAlarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "2"
  metric_name = "DiskSpaceUtilization"
  namespace = "System/Linux"
  period = "300"
  statistic = "Average"
  threshold = "80.0"
  alarm_actions = ["${var.sns_arn}"]
  dimensions {"MountPath" = "/", "InstanceId" = "${var.jump_server_id}", "Filesystem" = "/dev/xvde1"}
}

resource "aws_cloudwatch_metric_alarm" "JumpStatusCheckAlarm" {
  alarm_name = "hello-world${var.env}JumpStatusCheckAlarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "1"
  metric_name = "StatusCheckFailed"
  namespace = "AWS/EC2"
  period = "180"
  statistic = "Maximum"
  threshold = "1.0"
  alarm_actions = ["${var.sns_arn}"]
  dimensions {"InstanceId" = "${var.jump_server_id}"}
}

resource "aws_cloudwatch_metric_alarm" "High-DB-Connections" {
  alarm_name = "awsrds-${var.db_name}-High-DB-Connections"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "1"
  metric_name = "DatabaseConnections"
  namespace = "AWS/RDS"
  period = "300"
  statistic = "Average"
  threshold = "10000.0"
  alarm_actions = ["${var.sns_arn}"]
  dimensions {"DBInstanceIdentifier" = "${var.db_name}"}
}

resource "aws_cloudwatch_metric_alarm" "DB-High-Queue-Depth" {
  alarm_name = "awsrds-${var.db_name}-High-Queue-Depth"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "1"
  metric_name = "DiskQueueDepth"
  namespace = "AWS/RDS"
  period = "300"
  statistic = "Average"
  threshold = "4.0"
  alarm_actions = ["${var.sns_arn}"]
  dimensions {"DBInstanceIdentifier" = "${var.db_name}"}
}

resource "aws_cloudwatch_metric_alarm" "DB-High-Read-IOPS" {
  alarm_name = "awsrds-${var.db_name}-High-Read-IOPS"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "1"
  metric_name = "ReadIOPS"
  namespace = "AWS/RDS"
  period = "300"
  statistic = "Average"
  threshold = "800.0"
  alarm_actions = ["${var.sns_arn}"]
  dimensions {"DBInstanceIdentifier" = "${var.db_name}"}
}

resource "aws_cloudwatch_metric_alarm" "DB-High-Read-Latency" {
  alarm_name = "awsrds-${var.db_name}-High-Read-Latency"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "3"
  metric_name = "ReadLatency"
  namespace = "AWS/RDS"
  period = "300"
  statistic = "Average"
  threshold = "0.004"
  alarm_actions = ["${var.sns_arn}"]
  dimensions {"DBInstanceIdentifier" = "${var.db_name}"}
}

resource "aws_cloudwatch_metric_alarm" "DB-High-Read-Throughput" {
  alarm_name = "awsrds-${var.db_name}-High-Read-Throughput"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "1"
  metric_name = "ReadThroughput"
  namespace = "AWS/RDS"
  period = "300"
  statistic = "Average"
  threshold = "10485760.0"
  alarm_actions = ["${var.sns_arn}"]
  dimensions {"DBInstanceIdentifier" = "${var.db_name}"}
}

resource "aws_cloudwatch_metric_alarm" "DB-High-Write-IOPS" {
  alarm_name = "awsrds-${var.db_name}-High-Write-IOPS"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "5"
  metric_name = "WriteIOPS"
  namespace = "AWS/RDS"
  period = "300"
  statistic = "Average"
  threshold = "1500.0"
  alarm_actions = ["${var.sns_arn}"]
  dimensions {"DBInstanceIdentifier" = "${var.db_name}"}
}

resource "aws_cloudwatch_metric_alarm" "DB-Very-High-Write-IOPS" {
  alarm_name = "awsrds-${var.db_name}-Very-High-Write-IOPS"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "1"
  metric_name = "WriteIOPS"
  namespace = "AWS/RDS"
  period = "300"
  statistic = "Average"
  threshold = "4500.0"
  alarm_actions = ["${var.sns_arn}"]
  dimensions {"DBInstanceIdentifier" = "${var.db_name}"}
}

resource "aws_cloudwatch_metric_alarm" "DB-High-Write-Latency" {
  alarm_name = "awsrds-${var.db_name}-High-Write-Latency"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "3"
  metric_name = "WriteLatency"
  namespace = "AWS/RDS"
  period = "300"
  statistic = "Average"
  threshold = "0.04"
  alarm_actions = ["${var.autoscaling_arn}", "${var.sns_arn}"]
  dimensions {"DBInstanceIdentifier" = "${var.db_name}"}
}

resource "aws_cloudwatch_metric_alarm" "DB-High-Write-Throughput" {
  alarm_name = "awsrds-${var.db_name}-High-Write-Throughput"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "1"
  metric_name = "WriteThroughput"
  namespace = "AWS/RDS"
  period = "300"
  statistic = "Average"
  threshold = "41943040.0"
  alarm_actions = ["${var.sns_arn}"]
  dimensions {"DBInstanceIdentifier" = "${var.db_name}"}
}

resource "aws_cloudwatch_metric_alarm" "DatabaseCPUAlarm" {
  alarm_name = "hello-world-${var.env}-DatabaseCPUAlarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "2"
  metric_name = "CPUUtilization"
  namespace = "AWS/RDS"
  period = "300"
  statistic = "Average"
  threshold = "70.0"
  alarm_actions = ["${var.sns_arn}"]
  dimensions {"DBInstanceIdentifier" = "${var.db_name}"}
}

resource "aws_cloudwatch_metric_alarm" "app-elb-HTTPCode_Backend_4XX" {
  alarm_name = "hello-world-${var.env}-app-elb-HTTPCode_Backend_4XX"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "1"
  metric_name = "HTTPCode_Backend_4XX"
  namespace = "AWS/ELB"
  period = "60"
  statistic = "Average"
  threshold = "10.0"
  alarm_actions = ["${var.sns_arn}"]
  dimensions {"LoadBalancerName" = "${var.loadbalancer_name}"}
}

resource "aws_cloudwatch_metric_alarm" "app-elb-HTTPCode_Backend_5XX" {
  alarm_name = "hello-world-${var.env}-app-elb-HTTPCode_Backend_5XX"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "2"
  metric_name = "HTTPCode_Backend_5XX"
  namespace = "AWS/ELB"
  period = "60"
  statistic = "Average"
  threshold = "10.0"
  alarm_actions = ["${var.sns_arn}"]
  dimensions {"LoadBalancerName" = "${var.loadbalancer_name}"}
}

resource "aws_cloudwatch_metric_alarm" "app-elb-RequestCount" {
  alarm_name = "hello-world-${var.env}-app-elb-RequestCount"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "1"
  metric_name = "RequestCount"
  namespace = "AWS/ELB"
  period = "300"
  statistic = "Average"
  threshold = "75000.0"
  alarm_actions = ["${var.autoscaling_arn}", "${var.sns_arn}"]
  dimensions {"LoadBalancerName" = "${var.loadbalancer_name}"}
}

resource "aws_cloudwatch_metric_alarm" "DB-BinLogDiskUsage" {
  alarm_name = "${var.db_name}-BinLogDiskUsage"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "15"
  metric_name = "BinLogDiskUsage"
  namespace = "AWS/RDS"
  period = "60"
  statistic = "Average"
  threshold = "1000000000.0"
  alarm_actions = ["${var.sns_arn}"]
  dimensions {"DBInstanceIdentifier" = "${var.db_name}"}
}

resource "aws_cloudwatch_metric_alarm" "DB-CPUUtilization" {
  alarm_name = "${var.db_name}-CPUUtilization"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "10"
  metric_name = "CPUUtilization"
  namespace = "AWS/RDS"
  period = "60"
  statistic = "Average"
  threshold = "70.0"
  alarm_actions = ["${var.sns_arn}"]
  dimensions {"DBInstanceIdentifier" = "${var.db_name}"}
}

resource "aws_cloudwatch_metric_alarm" "DB-FreeStorageSpace" {
  alarm_name = "${var.db_name}-FreeStorageSpace"
  comparison_operator = "LessThanThreshold"
  evaluation_periods = "1"
  metric_name = "FreeStorageSpace"
  namespace = "AWS/RDS"
  period = "3600"
  statistic = "Average"
  threshold = "100000000000.0"
  alarm_actions = ["${var.sns_arn}"]
  dimensions {"DBInstanceIdentifier" = "${var.db_name}"}
}

resource "aws_cloudwatch_metric_alarm" "db-swap-usage" {
  alarm_name = "${var.db_name}-swap-usage"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "1"
  metric_name = "SwapUsage"
  namespace = "AWS/RDS"
  period = "300"
  statistic = "Average"
  threshold = "1.0"
  alarm_actions = ["${var.sns_arn}"]
  dimensions {"DBInstanceIdentifier" = "${var.db_name}"}
}

resource "aws_cloudwatch_metric_alarm" "file-services-bucket-size" {
  alarm_name = "hello-world${var.env}-file-services-bucket-size"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "1"
  metric_name = "BucketSizeBytes"
  namespace = "AWS/S3"
  period = "86400"
  statistic = "Average"
  threshold = "100000000000.0"
  alarm_actions = ["${var.sns_arn}"]
  dimensions {"StorageType" = "StandardStorage", "BucketName" = "${var.s3_bucket_name}"}
}
