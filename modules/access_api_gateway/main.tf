resource "aws_security_group_rule" "http" {
  type        = "ingress"
  description = "${var.description}"
  from_port   = "80"
  to_port     = "80"
  protocol    = "tcp"
  cidr_blocks = "${var.cidr_blocks}"
  security_group_id = "${var.sec_group_id}"
}

resource "aws_security_group_rule" "application_port" {
  type        = "ingress"
  description = "${var.description}"
  from_port   = "3000"
  to_port     = "3000"
  protocol    = "tcp"
  cidr_blocks = "${var.cidr_blocks}"
  security_group_id = "${var.sec_group_id}"
}
