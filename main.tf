terraform {
  required_providers {
    aws = {
      version = "~> 2.0"
      source = "hashicorp/aws"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "~> 3.1"
    }
  }
  backend "http" {}
}

provider "aws" {
  region = var.region
}

provider "gitlab" {
  token = var.gitlab_access_token
}