variable "env" {
  default = "Dev"
}

variable "vpc_id" {
  default = "vpc-7dbdfb04"
}


## CoreVPC environment variable related ##########################################################

variable "corevpc_environment_s3_bucket" {
  default = "sb-terraform-environments"
}

variable "corevpc_environment_s3_storage_class" {
  default = "STANDARD"
}

variable "corevpc_vars" {
  description = "Placeholder for real env vars that should not live inside of git repo. Use a var file instead"
  type = "map"
  default = {}
}


## EC2 specific variables ########################################################################

variable "ami_id" {
  default = ""
}

variable "app_instance_type" {
  default = "c4.2xlarge"
}

# Cheapest instance type allowed in the VPC unfortantely
variable "jump_instance_type" {
  default = "m3.medium"
}

variable "ssh_key_name" {
  default = "sb-bsr-sandbox-20180529"
}

variable "app_instance_iam_profile" {
  default = "server-nonprod"
}

variable "private_subnet_ids" {
  type = "list"
  default = ["subnet-63660839", "subnet-af1038e7", "subnet-e42650c8"]
  description = "App instances and DB go here"
}

variable "public_subnet_ids" {
  type = "list"
  default = ["subnet-8a6709d0", "subnet-7ee8c036", "subnet-d6384efa"]
  description = "Jump server goes here"
}

variable "max_instances" {
  default = 10
}

variable "min_instances" {
  default = 2
}

variable "desired_instances" {
  default = 2
}

variable "autoscaling_percent_capacity_increase" {
  default = "20"
}

variable "elb_access_logging_bucket" {
  default = "logs.cmswibeneficiaryuilogs"
}

variable "launch_config_user_data" {
  default = "return 0"
}

variable "gold_image_ami_id" {
  default = "ami-0868a91bb0f75c36e"
}

variable "gold_ami_description" {
  default = "EAST-RH 7-6 Gold Image V.1.06 (HVM) 03-27-19"
}

variable "wait_for_elb_capacity" {
  default = 2
}

variable "healthcheck_endpoint" {
  default = "/api/submissions/web-interface/health-br"
}

variable "open_http_security_group_id" {
 default = "sg-10f56860"
}

## RDS specific variables ########################################################################

variable "db_allocated_storage_size" {
  default = "500"
}

variable "mysql_version" {
  default = "5.7.19"
}

variable "db_instance_class" {
  default = "db.r4.4xlarge"
}

variable "db_snapshot_id" {
  default = ""
}

variable "db_subnet_group_name" {
  default = "qpp-bsr-api-dev-rdssubnetgroup"
}

variable "parameter_group_name" {
  default = "mysql57-cmswi"
}

variable "db_backup_retention_period" {
  default = "35"
}

variable "db_backup_window" {
  default = "05:00-06:00"
}

variable "db_copy_tags_to_snapshot" {
  default = "false"
}

variable "db_iops" {
  default = 5000
}

variable "db_kms_key_id" {
  default = "arn:aws:kms:us-east-1:375727523534:key/8e8b9029-8d1c-47bd-8632-9516cc732358"
}

variable "db_maintenance_window" {
  default = "sun:07:00-sun:08:00"
}

variable db_identifier {
  default = "qppbsrapidevdb"
}

variable "db_multi_az" {
  default = "false"
}


## SNS specific variables ########################################################################

# Higher environments
# https://events.pagerduty.com/integration/c1d83c07a1c04da9bc8eb930e34c8df7/enqueue

# Lower environments
# https://events.pagerduty.com/integration/7386ccb02632479dabdd27447121ae5e/enqueue

variable "pagerduty_url_for_QPP-WI-DEVOPS" {
  default  = "https://events.pagerduty.com/integration/7386ccb02632479dabdd27447121ae5e/enqueue"
}

variable "pagerduty_email_for_QPP-WI-DEVOPS-EMAIL" {
  default  = "qpp-wi-devops-lower-email.n7s8535c@cms-qpp.pagerduty.com"
}


## S3 specific variables #########################################################################

variable "s3_bucket_name" {
  default = "qpp-cmswi-services"
}

variable "kms_encryption_key_arn" {
  default = "arn:aws:kms:us-east-1:375727523534:key/a3ab8a6b-a923-4a7d-b1e7-a24a0d4a990f"
  description = "cmswi-s3 KMS key"
}

variable "replica_kms_encryption_key_arn" {
  default = "arn:aws:kms:us-east-1:375727523534:key/2385a3ae-62dd-4c76-91f8-84416285a1b9"
  description = "cmswi-s3-us-east-2 KMS key"
}

variable "s3_replication_role_arn" {
  default = "arn:aws:iam::375727523534:role/service-role/s3crr_role_for_qpp-cmswi-services_to_qpp-cmswi-services-replicat"
}


## Redis specific variables #######################################################################

variable "redis_ami_id" {
  default = "ami-091f4a896633392d3"
  description = "Packer image created specifically with Redis setup"
}

variable "redis_instance_type" {
  default = "m5.large"
}

variable "redis_subnet_id" {
  default = "subnet-63660839"
}

variable "redis_cluster_instance_count" {
  default = "3"
}

## ETL-specific variables ##########################################################################

variable "etl_instance_type" {
  default = "m4.xlarge"
}

variable "etl_subnet_id" {
  default = "subnet-63660839"
}
