provider "aws" {
  region = "us-east-1"
  version = "~> 1.10"
}

provider "aws" {
  region = "us-east-2"
  version = "~> 1.10"
  alias = "us-east-2"
}

terraform {
  backend "s3" {
    bucket     = "iac-terraform-environments"
    key        = "${var.env}/terraform/terraform.tfstate"
    region     = "us-east-1"
    lock_table = "tf_lock"
  }

  required_version = ">= 0.11.3"
}

module "s3" {
  source = "../../modules/s3"
  env = "${var.env}"
  bucket_name = "${var.s3_bucket_name}"
  encryption_key_arn = "${var.kms_encryption_key_arn}"
  replica_encryption_key_arn = "${var.replica_kms_encryption_key_arn}"
  iam_role_arn = "${var.s3_replication_role_arn}"
}

module "sqs" {
  source = "../../modules/sqs"
  env = "${var.env}"
}

module "db" {
  source = "../../modules/db"
  allocated_storage_size = "${var.db_allocated_storage_size}"
  mysql_version = "${var.mysql_version}"
  instance_class = "${var.db_instance_class}"
  snapshot_id = "${var.db_snapshot_id}"
  subnet_group_name = "${var.db_subnet_group_name}"
  parameter_group_name = "${var.parameter_group_name}"
  backup_retention_period = "${var.db_backup_retention_period}"
  backup_window = "${var.db_backup_window}"
  copy_tags_to_snapshot = "${var.db_copy_tags_to_snapshot}"
  iops = "${var.db_iops}"
  kms_key_id = "${var.db_kms_key_id}"
  maintenance_window = "${var.db_maintenance_window}"
  vpc_id = "${var.vpc_id}"
  env = "${var.env}"
  db_instance_subnet_ids = ["${var.private_subnet_ids}"]
  identifier = "${var.db_identifier}"
  multi_az = "${var.db_multi_az}"
}

module "app" {
  source = "../../modules/app/"
  env = "${var.env}"
  ami_id = "${var.ami_id}"
  app_instance_type = "${var.app_instance_type}"
  ssh_key_name = "${var.ssh_key_name}"
  app_instance_iam_profile = "${var.app_instance_iam_profile}"
  app_instance_subnet_ids = "${var.private_subnet_ids}"
  jump_server_subnet_ids = "${var.public_subnet_ids}"
  vpc_id = "${var.vpc_id}"
  max_instances = "${var.max_instances}"
  min_instances = "${var.min_instances}"
  desired_instances = "${var.desired_instances}"
  percent_capacity_increase = "${var.autoscaling_percent_capacity_increase}"
  jump_instance_type = "${var.jump_instance_type}"
  sg_database_id = "${module.db.aws_security_group_sg_database_id}"
  elb_logging_bucket = "${var.elb_access_logging_bucket}"
  user_data = "${var.launch_config_user_data}"
  gold_ami_id = "${var.gold_image_ami_id}"
  gold_ami_description = "${var.gold_ami_description}"
  autoscale_group_wait ="${var.wait_for_elb_capacity}"
  healthcheck_url = "${var.healthcheck_endpoint}"
  open_http_security_group_id = "${var.open_http_security_group_id}"
  private_subnet_ids = "${var.private_subnet_ids}"
}

module "sns" {
  source = "../../modules/sns"
  URL_QPP-WI-DEVOPS-PAGERDUTY = "${var.pagerduty_url_for_QPP-WI-DEVOPS}"
  email_QPP-WI-DEVOPS-PAGERDUTY = "${var.pagerduty_email_for_QPP-WI-DEVOPS-EMAIL}"
  env = "${var.env}"
}

module "cloudwatch" {
  source = "../../modules/cloudwatch"
  env = "${var.env}"
  autoscaling_arn = "${module.app.aws_autoscaling_policy_percent_capacity_arn}"
  sns_arn = "${module.sns.aws_sns_topic_QPP-WI-DEVOPS_arn}"
  autoscaling_name = "${module.app.aws_autoscaling_group_name}"
  loadbalancer_name = "${module.app.aws_elastic_load_balancer_name}"
  jump_server_id = "${module.app.aws_instance_jump_server_id}"
  db_name = "${var.db_identifier}"
  deadletter_queue = "${module.sqs.deadletter_queue_name}"
  data_queue = "${module.sqs.data_queue_name}"
  file_process_queue = "${module.sqs.file_process_queue_name}"
  s3_bucket_name = "${var.s3_bucket_name}"
}

module "access" {
  source = "./access"
  private_key = "${var.ssh_key_name}"
  jumpserver_ip = "${module.app.elastic_ip_jumpbox}"
  jumpserver_sec_group_id = "${module.app.aws_security_group_jump_server_id}"
  database_sec_group_id = "${module.db.aws_security_group_sg_database_id}"
  app_sec_group_id = "${module.app.aws_security_group_app_security_id}"
}
